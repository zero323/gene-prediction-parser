#! /usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Maciej Szymkiewicz'

from bs4 import BeautifulSoup
from collections import namedtuple
from collections import OrderedDict
import argparse

def main():
    parser= argparse.ArgumentParser()
    parser.add_argument("-m", dest="genemark_file", type=str, default=None,
                        help=("GeneMark input file (html format)"))
    parser.add_argument("-i", dest="geneid_file", type=str, default=None,
            help=("GeneID input file (html format)"))
    parser.add_argument("-s", dest="genescan_file", type=str, default=None,
                        help=("GeneScan input file (html format)"))

    args = parser.parse_args()

    if args.geneid_file:
        print('geneid,"",start,end')
        gid = GeneIDParser(input_file=args.geneid_file)
        gid.parse()
        gid.print_output()

    if args.genescan_file:
        print('genescan,"",start,end')
        gsp = GeneScanParser(input_file=args.genescan_file)
        gsp.parse()
        gsp.print_output()

    if args.genemark_file:
        print('Genemark,"", start, end')
        gmp = GeneMarkParser(input_file=args.genemark_file)
        gmp.parse()
        gmp.print_output()


class PredictionParser:
    def __init__(self, input_file):
        self.predicted_genes = OrderedDict()
        with open(input_file) as fr:
            soup = BeautifulSoup(fr)
            self.lines = filter(
                lambda x: len(x) > 0,
                map(lambda x: x.strip(), soup.pre.text.split('\n')))


    def parse(self):
        self.sanity_check()
        for line in self.lines[self.OFFSET:]:
            # If it is end of interesting input, break

            if self.finish(line):
                break

            if self.drop_line(line):
                continue

            # Split lines into bits
            bits = line.strip().split()
            # Sanity check
            assert len(bits) in self.EXPECTED_LENGTH, 'Invalid len of bits'
            bits = bits[self.START: self.END]

            # Parse bits
            exon = self.get_exon(bits)
            self.predicted_genes.setdefault(self.get_key(exon.gene), []).append(exon)

    def print_output(self):
        for gene, prediction in self.get_predictions():
            print(
                '{0},{1},{2},{3}'.
                format(gene, prediction.strand, prediction.start, prediction.stop,))



    def sanity_check(self):
        raise NotImplementedError

    def get_key(self, gene):
        raise NotImplementedError

    def get_exon(self, bits):
        return self.FORMAT(*bits)

    def drop_line(self, line):
        raise NotImplementedError

    def finish(self, line):
        raise NotImplementedError

    def get_predictions(self):
        raise NotImplementedError

class GeneMarkParser(PredictionParser):
    OFFSET = 9
    START = 0
    END = 7
    EXPECTED_LENGTH = (11, )
    FORMAT = namedtuple('GeneMarkFormat',
                        ['gene', 'exon', 'strand', 'exon_type', 'start', 'stop', 'length'])

    def sanity_check(self):
        # Check if OFFSET is correct
        first = self.FORMAT(
            *self.lines[self.OFFSET].strip().split()[self.START:self.END])
        assert first.gene == u'1' and first.exon == u'1', 'Wrong offset'

    def get_key(self, gene):
        return gene

    def get_exon(self, bits):
        return self.FORMAT(*bits)

    def drop_line(self, line):
        return False

    def finish(self, line):
        if line.startswith('#'):
            return True

    def get_predictions(self):
        for key, list_of_values in self.predicted_genes.iteritems():
            yield (key, reduce(lambda x, y: self.FORMAT(
                gene=x.gene,
                exon=None,
                strand=x.strand,
                exon_type=x.exon_type,
                start=min(int(x.start), int(y.start)),
                stop=max(int(x.stop), int(y.stop)),
                length=-1
            ), list_of_values))


class GeneIDParser(PredictionParser):
    OFFSET = 6
    START = 0
    END = 9
    EXPECTED_LENGTH = (9, )
    FORMAT = namedtuple('GeneIDFormat',
                        ['sequence', 'source_app', 'exon_type', 'start', 'stop', 'score', 'strand', 'frame', 'gene'])

    def sanity_check(self):
        pass

    def get_key(self, gene):
        return gene.split('_')[-1]

    def get_exon(self, bits):
        return self.FORMAT(*bits)

    def drop_line(self, line):
        if line.startswith('#'):
            return True
        return False

    def finish(self, line):
        return False

    def get_predictions(self):
        for key, list_of_values in self.predicted_genes.iteritems():
            yield (key, reduce(lambda x, y: self.FORMAT(
                sequence=x.sequence,
                source_app=x.source_app,
                exon_type=x.exon_type,
                start=min(int(x.start), int(y.start)),
                stop=max(int(x.stop), int(y.stop)),
                score=float(x.score) + float(y.score),
                strand=x.strand,
                frame=x.frame,
                gene=x.gene,
            ), list_of_values))

class GeneScanParser(PredictionParser):
    OFFSET = 7
    START = 0
    END = 5
    EXPECTED_LENGTH = (7, 13)
    FORMAT = namedtuple('GeneScanFormat',
                        ['gene', 'exon_type', 'strand', 'start', 'stop'])

    def sanity_check(self):
        pass

    def get_key(self, gene):
        return gene.split('.')[0]

    def get_exon(self, bits):
        if bits[2] == u'-':
            bits[3], bits[4] = bits[4], bits[3]
        return self.FORMAT(*bits)

    def drop_line(self, line):
        if line.startswith('#'):
            return True

    def finish(self, line):
        if line.startswith('Suboptimal'):
            return True
        return False

    def get_predictions(self):
        for key, list_of_values in self.predicted_genes.iteritems():
           yield (key, reduce(lambda x, y: self.FORMAT(
                exon_type=x.exon_type,
                start=min(int(x.start), int(y.start)),
                stop=max(int(x.stop), int(y.stop)),
                strand=x.strand,
                gene=x.gene,
                ), list_of_values))


if __name__ == '__main__':
    main()




