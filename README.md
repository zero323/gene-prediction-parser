Simple script for parsing GeneID, GeneMark, GeneScan web pages written as a part of a course assingment. I wouldn't use it unless you are desperate. 

Example data from [geneid](http://genome.crg.es/software/geneid/index.html#ack), [GeneMark](http://exon.gatech.edu/GeneMark/) and [GeneScan](http://genes.mit.edu/GENSCAN.html).

